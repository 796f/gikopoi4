# README #

This repository is collection of patches for https://github.com/iccanobif/gikopoi2/


### Serving patches ###

* The patches are served in .diff files and asset folders implemented in shell script that will patch the main branch cloned from github
* We aim to make the patches compatible with main branch.
